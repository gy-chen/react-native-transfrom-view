import _ from 'lodash';
import React, { Component } from 'react';
import { StyleSheet, View, Text, Button } from 'react-native';
import { Constants } from 'expo';
import { TransfromView, TransfromImage } from './dist';
import Swiper from './components/Swiper';

interface Prop {
  
}

interface State {
  scene: Scene
}

type Scene = 'view'
  | 'image'
  | 'swiper';

type Data = {
  text: string
  color: string
  height: number
}

const data = _.chain(5)
  .range()
  .map((num) => ({
    text: `第${num}頁`,
    color: _.sample(['aliceblue', 'antiquewhite', 'aqua', 'aquamarine', 'azure', 'chocolate', 'darkblue', 'khaki']) || 'red',
    height: _.sample([1000, 500, 350, 750, 200]) || 500,
  }))
  .value();

class App extends Component<Prop, State> {
  state: State = {
    scene: 'view',
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header} >
          <Button color={this.state.scene === 'view' ? 'red' : undefined} title="TransfromView" onPress={() => this._handleButtonPress('view')} />
          <Button color={this.state.scene === 'image' ? 'red' : undefined} title="TransfromImage" onPress={() => this._handleButtonPress('image')} />
          <Button color={this.state.scene === 'swiper' ? 'red' : undefined} title="Swiper" onPress={() => this._handleButtonPress('swiper')} />
        </View>
        {this._renderByScene()}
      </View>
    );
  }

  _renderByScene = () => {
    switch (this.state.scene) {
      case 'view':
        return (
          <TransfromView wrapperStyle={{ flex: 1, backgroundColor: 'red', justifyContent: 'center', alignItems: 'center' }}>
            <Text style={styles.text} >Hello World</Text>
          </TransfromView>
        );
      case 'image':
        return (
          <TransfromImage source={{ uri: 'https://via.placeholder.com/350x150' }} />
        );
      case 'swiper':
        return (
          <Swiper horizontal data={data} renderItem={this._renderItem} />
        )
      default:
        return null
    }
  }

  _renderItem = (info: { item: Data; index: number; }) => {
    return (
      <View style={[styles.item, { height: info.item.height, backgroundColor: info.item.color }]} >
        <Text>{info.item.text}</Text>
      </View>
    );
  }

  _handleButtonPress = (scene: Scene) => {
    this.setState({ scene })
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ecf0f1',
  },
  header: {
    paddingTop: Constants.statusBarHeight,
    justifyContent: 'space-around',
    backgroundColor: '#ECEFF1',
    flexDirection: 'row'
  },
  text: {
    fontSize: 18,
    color: 'white',
  },
  item: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});

export default App;