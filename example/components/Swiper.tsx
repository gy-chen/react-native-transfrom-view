import React, {
  PureComponent,
} from 'react';
import {
  StyleSheet,
  View,
  ActivityIndicator,
  StyleProp,
} from 'react-native';
import {
  ViewStyle,
  LayoutRectangle,
  LayoutChangeEvent,
} from 'react-native'
import Carousel, {
  AdditionalParallaxProps,
} from 'react-native-snap-carousel';
import { TransfromView } from '../dist';

interface Prop<ItemT> {
  containerStyle?: StyleProp<ViewStyle>
  horizontal?: boolean
  data: ItemT[]
  renderItem: (info: { item: ItemT; index: number }, parallaxProps?: AdditionalParallaxProps) => React.ReactNode
  onSnapToItem?: (index: number) => void
}

interface State {
  index: number
  containerLayout?: LayoutRectangle
  scrollEnabled: boolean,
}

class Swiper<ItemT> extends PureComponent<Prop<ItemT>, State> {
  $carousel = React.createRef<Carousel<ItemT>>();

  state: State = {
    index: 0,
    scrollEnabled: true,
  }

  render() {
    const { containerStyle, ...props } = this.props;
    return (
      <View style={this._containerStyle} onLayout={this._handleContainerLayout}>
        {
          this.state.containerLayout === undefined
            ? <ActivityIndicator size="large" />
            : <Carousel<ItemT>
              ref={this.$carousel}
              data={props.data}
              inactiveSlideScale={1}
              renderItem={this._renderItem}
              itemWidth={this.state.containerLayout.width}
              sliderWidth={this.state.containerLayout.width}
              onSnapToItem={this.handleSnapToItem}
              scrollEnabled={this.state.scrollEnabled}
            />
        }
      </View>
    );
  }

  _renderItem = (info: { item: ItemT; index: number }, parallaxProps?: AdditionalParallaxProps) => {
    return (
      <TransfromView onGesturePanEnableChange={this.handleItemPanEnableChange}>
        {this.props.renderItem && this.props.renderItem(info)}
      </TransfromView>
    );
  }

  _handleContainerLayout = (event: LayoutChangeEvent) => {
    this.setState({
      containerLayout: event.nativeEvent.layout,
    });
  }

  handleSnapToItem = (index: number) => {
    this.props.onSnapToItem && this.props.onSnapToItem(index);

    this.setState({
      scrollEnabled: true,
    });
  }

  handleItemPanEnableChange = (enable: boolean) => {
    this.setState({
      scrollEnabled: !enable,
    });
  }

  private get _containerStyle() {
    return [
      styles.contianer,
      StyleSheet.flatten(this.props.containerStyle),
    ];
  }

  private get _itemWrapperStyle() {
    const { horizontal } = this.props;
    const { containerLayout } = this.state;

    return [
      styles.item,
      {
        width: containerLayout && containerLayout.width,
        height: horizontal && containerLayout ? containerLayout.height : undefined,
      },
    ];
  }

}

const styles = StyleSheet.create({
  contianer: {
    flex: 1,
    justifyContent: 'center',
  },
  item: {
    
  },
});

export default Swiper;