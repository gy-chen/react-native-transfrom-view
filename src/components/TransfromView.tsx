import React, {
  PureComponent, Ref,
} from 'react';
import {
  View,
  Animated,
  StyleSheet,
} from 'react-native';
import {
  StyleProp,
  ViewStyle,
  ViewProps,
  LayoutChangeEvent,
} from 'react-native';
import {
  TapGestureHandler,
  PanGestureHandler,
  PinchGestureHandler,
  State as GestureState,
} from 'react-native-gesture-handler';
import {
  PinchGestureHandlerGestureEvent,
  PinchGestureHandlerStateChangeEvent,
  PanGestureHandlerGestureEvent,
  PanGestureHandlerStateChangeEvent,
  TapGestureHandlerStateChangeEvent,
} from 'react-native-gesture-handler';

import Rectangle from '../utils/Rectangle';
import AnimatedValue from '../utils/AnimatedValue';
import _ from 'lodash';

export interface Prop extends ViewProps {
  containerStyle?: StyleProp<ViewStyle>
  wrapperStyle?: StyleProp<ViewStyle>
  minScale?: number
  maxScale?: number
  useNativeDriver?: boolean

  enabled?: boolean
  waitFor?: Ref<any> | Ref<any>[]
  simultaneousHandlers?: Ref<any> | Ref<any>[]

  onContentAlign?: (align: Align) => void
  onGesturePanEnableChange?: (enable: boolean) => void;
}

export interface State {
  enableScale: boolean
  enablePan: boolean
  enableDoubleTap: boolean
}

export default class TransformView extends PureComponent<Prop> {
  static defaultProps = {
    enabled: true,
  };

  $container = React.createRef<View>();
  $wrapper = React.createRef<View>();
  $tap = React.createRef<TapGestureHandler>();
  $pan = React.createRef<PanGestureHandler>();
  $pinch = React.createRef<PinchGestureHandler>();

  containerRectangle = new Rectangle();
  wrapperRectangle = new Rectangle();

  scale = new AnimatedValue(1);
  translateX = new AnimatedValue(0);
  translateY = new AnimatedValue(0);

  animatedParallel?: Animated.CompositeAnimation;

  state = {
    enablePinch: true,
    enablePan: false,
    enableDoubleTap: true,
  };

  componentDidMount() {
    this.scale.addListener('valueState', this.handleScaleStateChange);
  }

  componentWillUnmount() {
    this.scale.removeAllListener('valueState');
  }

  handleContainerLayout = (event: LayoutChangeEvent) => {
    this.containerRectangle.top = event.nativeEvent.layout.y;
    this.containerRectangle.left = event.nativeEvent.layout.x;
    this.containerRectangle.right = event.nativeEvent.layout.x + event.nativeEvent.layout.width;
    this.containerRectangle.bottom = event.nativeEvent.layout.y + event.nativeEvent.layout.height;
    this.wrapperRectangle.top = event.nativeEvent.layout.y;
    this.wrapperRectangle.left = event.nativeEvent.layout.x;
    this.wrapperRectangle.right = event.nativeEvent.layout.x + event.nativeEvent.layout.width;
    this.wrapperRectangle.bottom = event.nativeEvent.layout.y + event.nativeEvent.layout.height;
  };

  handlePinchAndPanGestureEvent = (event: PinchGestureHandlerGestureEvent | PanGestureHandlerGestureEvent) => {
    if ('scale' in event.nativeEvent) {
      this.scale.value = this.scale.valueState * event.nativeEvent.scale;
    }

    if ('translationX' in event.nativeEvent && 'translationY' in event.nativeEvent) {
      this.translateX.value = this.translateX.valueState + event.nativeEvent.translationX;
      this.translateY.value = this.translateY.valueState + event.nativeEvent.translationY;
    }
  };

  handlePinchAndPanHandlerStateChange = (event: PinchGestureHandlerStateChangeEvent | PanGestureHandlerStateChangeEvent) => {
    const { state, oldState } = event.nativeEvent;

    if (oldState === GestureState.ACTIVE) {
      if ('scale' in event.nativeEvent) {
        this.scale.value = this.scale.valueState = this.scale.valueState * event.nativeEvent.scale;
      }
      if ('translationX' in event.nativeEvent && 'translationY' in event.nativeEvent) {
        this.translateX.value = this.translateX.valueState = this.translateX.valueState + event.nativeEvent.translationX;
        this.translateY.value = this.translateY.valueState = this.translateY.valueState + event.nativeEvent.translationY;
      }
    }

    if (state === GestureState.END) {
      this.animated(this.transformBounce);
    }
  }

  handleDoubleTapStateChange = (event: TapGestureHandlerStateChangeEvent) => {
    const { state } = event.nativeEvent;
    
    if (state === GestureState.END) {
      this.animated(this.transformDoubleTap);
    }
  };

  handleScaleStateChange = (curr: number, prev: number) => {
    const { onGesturePanEnableChange } = this.props;
    const scaleIsZoomUp = _.round(curr, 5) > 1;

    if (scaleIsZoomUp != this.state.enablePan) {
      this.setState({
        enablePan: scaleIsZoomUp,
      }, () => {
        onGesturePanEnableChange && onGesturePanEnableChange(scaleIsZoomUp);
      });
    }
  }

  animated = _.debounce((transform: {scale?: number, translateX?: number, translateY?: number}) => {
    this.animatedParallel && this.animatedParallel.stop();

    const { scale, translateX, translateY } = transform;
    const list = [];
    
    if (scale !== undefined && scale !== this.scale.value) {
      this.scale.valueWithoutAnimate = this.scale.valueState = scale;
      list.push(Animated.timing(this.scale.animate, {
        toValue: this.scale.value,
        duration: 300,
        useNativeDriver: this.props.useNativeDriver,
      }));
    }
    
    if (translateX !== undefined && translateX !== this.translateX.value) {
      this.translateX.valueWithoutAnimate = this.translateX.valueState = translateX;
      list.push(Animated.timing(this.translateX.animate, {
        toValue: this.translateX.value,
        duration: 300,
        useNativeDriver: this.props.useNativeDriver,
      }));
    }

    if (translateY !== undefined && translateY !== this.translateY.value) {
      this.translateY.valueWithoutAnimate = this.translateY.valueState = translateY;
      list.push(Animated.timing(this.translateY.animate, {
        toValue: this.translateY.value,
        duration: 300,
        useNativeDriver: this.props.useNativeDriver,
      }));
    }

    this.animatedParallel = Animated.parallel(list);
    this.animatedParallel.start(() => {
      this.animatedParallel = undefined;
    })
  }, 50);

  generateTransformToRectangle = (baseRectangle: Rectangle,targetRectangle: Rectangle) => new Rectangle(
    baseRectangle.left + (targetRectangle.left - baseRectangle.left),
    baseRectangle.top + (targetRectangle.top - baseRectangle.top),
    baseRectangle.right + (targetRectangle.right - baseRectangle.right),
    baseRectangle.bottom + (targetRectangle.bottom - baseRectangle.bottom),
  );

  get containerStyle() {
    return [
      styles.container,
      StyleSheet.flatten(this.props.style),
      StyleSheet.flatten(this.props.containerStyle),
    ];
  }

  get wrapperStyle() {
    return [
      styles.wrapper,
      {
        transform: [
          { scale: this.scale.animate },
          { translateX: this.translateX.animate },
          { translateY: this.translateY.animate },
        ],
      },
      StyleSheet.flatten(this.props.wrapperStyle),
    ];
  }

  get transformBounce() {
    const containerRectangle = this.containerRectangle;
    const wrapperRectangle = this.wrapperRectangle;
    const translatedWrapperRectangle = this.translatedWrapperRectangle;

    let scale = 1;
    const currentScale = this.scale.value;
    const maxScale = this.props.maxScale || 3;
    const minScale = this.props.minScale || 1;
    if (currentScale > maxScale) {
      scale = maxScale / currentScale;
    } else if (currentScale < minScale) {
      scale = minScale / currentScale;
    }

    const targetRectangle = translatedWrapperRectangle
      .transformed({
        scale,
        translateX: 0,
        translateY: 0,
        pivotX: containerRectangle.centerX,
        pivotY: containerRectangle.centerY,
      })
      .aligned(containerRectangle);

    const transformToRectangle = this.generateTransformToRectangle(translatedWrapperRectangle, targetRectangle)
    
    this.props.onContentAlign && this.props.onContentAlign({
      top: transformToRectangle.top === containerRectangle.top,
      left: transformToRectangle.left === containerRectangle.left,
      right: transformToRectangle.right === containerRectangle.right,
      bottom: transformToRectangle.bottom === containerRectangle.bottom,
    });
    
    return wrapperRectangle.transform(transformToRectangle);
  }

  get transformDoubleTap() {
    const containerRectangle = this.containerRectangle;
    const wrapperRectangle = this.wrapperRectangle;
    const translatedWrapperRectangle = this.translatedWrapperRectangle;

    let scale = this.scale.value;
    let pivotX = containerRectangle.centerX;
    let pivotY = containerRectangle.centerY;
    const maxScale = this.props.maxScale || 3;
    const minScale = this.props.minScale || 1;
    if (scale > (1 + maxScale) / 2) {
      scale = minScale / scale;
    } else {
      scale = maxScale / scale;
    }

    const targetRectangle = translatedWrapperRectangle
      .transformed({
        scale,
        translateX: 0, translateY: 0,
        pivotX, pivotY,
      })
      .transformed({
        scale: 1,
        translateX: containerRectangle.centerX - pivotX,
        translateY: containerRectangle.centerY - pivotY,
      })
      .aligned(containerRectangle);

    return wrapperRectangle.transform(this.generateTransformToRectangle(translatedWrapperRectangle, targetRectangle));
  }

  get translatedWrapperRectangle() {
    return this.wrapperRectangle.transformed(this.transform);
  }

  get transform(): Transform {
    return {
      scale: this.scale.value,
      translateX: this.translateX.value,
      translateY: this.translateY.value,
    };
  }

  generateWaitFor = (refs?: Ref<any> | Ref<any>[]): Ref<any> | Ref<any>[] | undefined => {
    return _.chain([]).concat(this.props.waitFor || [], refs || []).compact().value();
  }

  generateSimultaneousHandlers = (refs?: Ref<any> | Ref<any>[]): Ref<any> | Ref<any>[] | undefined => {
    return _.chain([]).concat(this.props.simultaneousHandlers || [], refs || []).compact().value();
  }

  render() {
    const {
      style, containerStyle, wrapperStyle,
      enabled, waitFor, simultaneousHandlers,
      children,
      ...props
    } = this.props;

    const {
      enableDoubleTap,
      enablePan,
      enablePinch,
    } = this.state;

    return (
      <View ref={this.$container}
        style={this.containerStyle}
        onLayout={this.handleContainerLayout} >
        <TapGestureHandler ref={this.$tap}
          enabled={enabled && enableDoubleTap}
          waitFor={this.generateWaitFor([this.$pan, this.$pinch])}
          simultaneousHandlers={this.generateSimultaneousHandlers()}
          numberOfTaps={2}
          onHandlerStateChange={this.handleDoubleTapStateChange} >
          <PinchGestureHandler ref={this.$pinch}
            enabled={enabled && enablePinch}
            waitFor={this.generateWaitFor()}
            simultaneousHandlers={this.generateSimultaneousHandlers([this.$pan])}
            shouldCancelWhenOutside={true}
            onGestureEvent={this.handlePinchAndPanGestureEvent}
            onHandlerStateChange={this.handlePinchAndPanHandlerStateChange} >
            <PanGestureHandler ref={this.$pan}
              enabled={enabled && enablePan}
              waitFor={this.generateWaitFor()}
              simultaneousHandlers={this.generateSimultaneousHandlers([this.$pinch])}
              shouldCancelWhenOutside={true}
              onGestureEvent={this.handlePinchAndPanGestureEvent}
              onHandlerStateChange={this.handlePinchAndPanHandlerStateChange} >
              <Animated.View {...props}
                ref={this.$wrapper}
                style={this.wrapperStyle} >
                {children}
              </Animated.View>
            </PanGestureHandler>
          </PinchGestureHandler>
        </TapGestureHandler>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  wrapper: {
    flex: 1,
    justifyContent: 'center',
  },
});

export interface Transform {
  scale: number
  translateX: number
  translateY: number
}

export interface Align {
  top: boolean
  left: boolean
  right: boolean
  bottom: boolean
}