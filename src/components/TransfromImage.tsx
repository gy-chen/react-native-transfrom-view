import React, { PureComponent } from 'react';
import TransfromView, { Prop as TransfromViewProp } from './TransfromView';
import { View, Image, ActivityIndicator, StyleSheet } from 'react-native';
import { StyleProp, ViewStyle, ImageStyle, ImageProps, ImageRequireSource, ImageURISource, LayoutChangeEvent, LayoutRectangle } from 'react-native';

interface Prop extends ImageProps, TransfromViewProp {
  style?: StyleProp<ViewStyle>
  imageContainerStyle?: StyleProp<ViewStyle>
  imageStyle?: StyleProp<ImageStyle>
  indicatorStyle?: StyleProp<ViewStyle>
  align?: 'auto' | 'width' | 'height'
}

interface State {
  loading: boolean
  width?: number | string
  height?: number | string
  align?: 'width' | 'height'
}

class TransfromImage extends PureComponent<Prop, State> {
  static defaultProps: Partial<Prop> = {
    align: 'auto',
  }

  _containerLayout?: LayoutRectangle;

  state: State = {
    loading: true,
  }

  render() {
    const {
      style, containerStyle, wrapperStyle,
      children,
      ...props
    } = this.props;

    return (
      <TransfromView {...{ style: [style, { alignItems: this.state.align === 'height' ? 'center' : undefined, }], containerStyle, wrapperStyle }} >
        <View style={this._containerStyle} onLayout={this._handleLayout} >
          {
            this.state.loading === true
            ? <ActivityIndicator style={this._indicatorStyle} size="large" animating />
            : <Image {...props} style={this._imageStyle} />
          }
        </View>
      </TransfromView>
    )
  }

  private _handleLayout = (event: LayoutChangeEvent) => {
    if (this.state.height !== undefined && this.state.width !== undefined) {
      this.props.onLayout && this.props.onLayout(event);
      return;
    }
    
    const uri = this.props.source !== undefined
      && typeof (this.props.source as ImageRequireSource) !== 'number'
      && (this.props.source as ImageURISource[]).hasOwnProperty('length') === false
      && (this.props.source as ImageURISource).hasOwnProperty('uri') === true
        ? (this.props.source as ImageURISource).uri
        : undefined;
    if (event.nativeEvent === undefined || event.nativeEvent.layout === undefined) return;

    this._containerLayout = event.nativeEvent.layout;
    if (uri !== undefined) {
      Image.getSize(uri, (w: number, h: number) => {
        if (this._containerLayout === undefined) return;
        const _align = w >= h ? 'width' : 'height';

        if (
          (this.props.align === 'auto' && _align === 'width')
          ||
          this.props.align === 'width'
        ) {
          this.setState({
            width: this._containerLayout.width,
            height: this._containerLayout.width * (h / w),
            align: 'width',
            loading: false,
          });
        } else if (
          (this.props.align === 'auto' && _align === 'height')
          ||
          this.props.align === 'height'
        ) {
          this.setState({
            height: this._containerLayout.height,
            width: this._containerLayout.height * (w / h),
            align: 'height',
            loading: false,
          });
        }
      });
    } else if (this.props.width !== undefined && this.props.height !== undefined) {
      this.setState({
        width: this.props.width,
        height: this.props.height,
        loading: false,
      });
    }
  };

  private get _containerStyle() {
    const hasLayout = this.state.height !== undefined && this.state.width !== undefined;

    return [
      styles.container,
      {
        flex: !hasLayout ? 1 : undefined,
        width: hasLayout ? this.state.width : undefined,
        height: hasLayout ? this.state.height : undefined,
      },
      StyleSheet.flatten(this.props.containerStyle),
    ];
  }

  private get _imageStyle() {
    const hasLayout = this.state.height !== undefined && this.state.width !== undefined;

    return [
      styles.image,
      {
        width: hasLayout ? this.state.width : undefined,
        height: hasLayout ? this.state.height : undefined,
      },
      StyleSheet.flatten(this.props.imageStyle),
    ];
  }

  private get _indicatorStyle() {
    return [
      styles.indicator,
      StyleSheet.flatten(this.props.indicatorStyle),
    ];
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
  },
  indicator: {
    flex: 1,
    height: 500,
  },
  image: {
    flex: 1,
    resizeMode: 'contain',
  },
});

export default TransfromImage;